package com.example;

import com.example.paint.BucketFill;
import com.example.paint.Canvas;
import com.example.paint.Line;
import com.example.paint.Rectangle;

import java.util.Objects;
import java.util.Scanner;

import static java.lang.Integer.parseInt;

public class Main {
    private static final String SPLITTER = " ";

    public static void main(String[] args) {
        boolean exit = false;
        Canvas canvas = null;
        Scanner scanner = new Scanner(System.in);

        while (!exit) {
            System.out.println("enter command:");
            String cmdStr = scanner.hasNextLine() ? scanner.nextLine() : "";
            String[] cmd = cmdStr.split(SPLITTER);
            try {
                switch (cmd[0].toUpperCase()) {
                    case "C" -> {
                        canvas = Canvas.getInstance(parseInt(cmd[1]), parseInt(cmd[2]));
                        Objects.requireNonNull(canvas).draw();
                    }
                    case "L" -> {
                        Line l = new Line(parseInt(cmd[1]), parseInt(cmd[2]),
                                parseInt(cmd[3]), parseInt(cmd[4]));
                        l.draw(canvas);
                    }
                    case "R" -> {
                        Rectangle r = new Rectangle(parseInt(cmd[1]), parseInt(cmd[2]),
                                parseInt(cmd[3]), parseInt(cmd[4]));
                        r.draw(canvas);
                    }
                    case "B" -> {
                        BucketFill f = new BucketFill(parseInt(cmd[1]), parseInt(cmd[2]),
                                cmd[3].charAt(0));
                        f.draw(canvas);
                    }
                    case "Q" -> exit = true;
                }
            } catch (IndexOutOfBoundsException | NumberFormatException | NullPointerException ex) {
                System.out.println("Invalid command");
            }

        }
    }
}
