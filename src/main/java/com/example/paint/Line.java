package com.example.paint;

public class Line extends Base implements Shape {
    private final Point start;
    private final Point end;

    public Line(int startX, int startY, int endX, int endY) {
        this.start = new Point(startX, startY, this.character);
        this.end = new Point(endX, endY, this.character);
    }

    @Override
    public void draw(Canvas canvas) {
        if (canvas == null) {
            return;
        }
        if (this.start.x == this.end.x) {
            drawVertical(canvas);
            return;
        }
        if (this.start.y == this.end.y) {
            drawHorizontal(canvas);
        }
    }

    private void drawHorizontal(Canvas canvas) {
        int startX = Math.min(this.start.x, this.end.x);
        int endX = Math.max(this.start.x, this.end.x);
        for (int x = startX; x <= endX; x++) {
            canvas.setValue(x, this.start.y, this.character);
        }
        canvas.draw();
    }

    private void drawVertical(Canvas canvas) {
        int startY = Math.min(this.start.y, this.end.y);
        int endY = Math.max(this.start.y, this.end.y);
        for (int y = startY; y <= endY; y++) {
            canvas.setValue(this.start.x, y, this.character);
        }
        canvas.draw();
    }
}
