package com.example.paint;

class Point extends Base {
    final int x;
    final int y;

    public Point(int x, int y, char c) {
        super(c);
        this.x = x;
        this.y = y;
    }
}
