package com.example.paint;

import java.util.ArrayList;
import java.util.List;

public class BucketFill extends Base implements Shape {
    private final Point startPoint;

    public BucketFill(int x, int y, Character character) {
        super(character);
        this.startPoint = new Point(x, y, this.character);
    }

    @Override
    public void draw(Canvas canvas) {
        if (canvas == null) {
            return;
        }
        List<Point> points = new ArrayList<>();
        points.add(startPoint);

        while (points.size() > 0) {
            Point p = points.remove(0);
            Character symbol = canvas.getValue(p.x, p.y);
            if (symbol != null && !symbol.equals(BORDER_SYMBOL) && !symbol.equals(this.character)) {
                canvas.setValue(p.x, p.y, this.character);

                points.add(new Point(p.x + 1, p.y, this.character));
                points.add(new Point(p.x - 1, p.y, this.character));
                points.add(new Point(p.x, p.y + 1, this.character));
                points.add(new Point(p.x, p.y - 1, this.character));
            }
        }
        canvas.draw();
    }
}
