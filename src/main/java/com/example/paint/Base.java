package com.example.paint;

/**
 * The basic geometry class which holds generic actions
 */
abstract class Base {
    protected Character character;
    protected final Character BORDER_SYMBOL = 'x';

    /**
     * Constructs a new geometry instance.
     * @param character The symbol which will be drawn
     */
    public Base(Character character) {
        this.character = character;
    }

    /**
     * Constructs a new geometry instance.
     * The symbol will be BORDER_SYMBOL
     */
    public Base() {
        this.character = BORDER_SYMBOL;
    }

}
