package com.example.paint;

public class Rectangle extends Base implements Shape {
    private final Point topLeft;
    private final Point bottomRight;

    public Rectangle(int topLeftX, int topLeftY, int bottomRightX, int bottomRightY) {
        this.topLeft = new Point(topLeftX, topLeftY, this.character);
        this.bottomRight = new Point(bottomRightX, bottomRightY, this.character);
    }

    @Override
    public void draw(Canvas canvas) {
        if (canvas == null) {
            return;
        }
        Point[] realPosition = getRealPosition();
        Point topLeft = realPosition[0];
        Point bottomRight = realPosition[1];
        if (topLeft == null || bottomRight == null) {
            return;
        }

        for (int i = topLeft.y; i <= bottomRight.y; i++) {
            canvas.setValue(topLeft.x, i, this.character);
            canvas.setValue(bottomRight.x, i, this.character);
        }
        for (int i = topLeft.x + 1; i < bottomRight.x ; i++) {
            canvas.setValue(i, topLeft.y, this.character);
            canvas.setValue(i, bottomRight.y, this.character);
        }
        canvas.draw();
    }

    private Point[] getRealPosition() {
        Point topLeft = null;
        Point bottomRight = null;
        if (this.topLeft.x < this.bottomRight.x && this.topLeft.y < this.bottomRight.y) {
            topLeft = new Point(this.topLeft.x, this.topLeft.y, this.topLeft.character);
            bottomRight = new Point(this.bottomRight.x, this.bottomRight.y, this.bottomRight.character);
        } else if (this.topLeft.x > this.bottomRight.x && this.topLeft.y > this.bottomRight.y) {
            bottomRight = new Point(this.topLeft.x, this.topLeft.y, this.topLeft.character);
            topLeft = new Point(this.bottomRight.x, this.bottomRight.y, this.bottomRight.character);
        }
        return new Point[]{topLeft, bottomRight};
    }
}
