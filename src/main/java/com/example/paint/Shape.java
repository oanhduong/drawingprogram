package com.example.paint;

/**
 * This interface imposes drawing actions on the objects of each class that implements it
 */
public interface Shape {

    /**
     *	Each of shape must have its own draw implementation
     *	together with its attributes. So this function will be
     *	overridden by concrete class (a specific shape) to draw
     *	the shape in given canvas
     *
     * @param canvas the context in which object will be drawn
     * This function returns the previous state of
     * canvas and its content without throwing message
     * or exception if command is invalid.
     */
    void draw(Canvas canvas);
}
