package com.example.paint;

public class Canvas {
    private final int width;
    private final int height;
    private final Character[][] matrix;
    private static final Character ROW_CHAR = '-';
    private static final Character COL_CHAR = '|';
    private static final Character BLANK_CHAR = ' ';

    public static Canvas getInstance(int width, int height) {
        if (width <= 0 || height <= 0) {
            return null;
        }
        return new Canvas(width, height);
    }

    private Canvas(int width, int height) {
        this.width = width + 2;
        this.height = height + 2;
        this.matrix = new Character[this.height][this.width];
        this.initMatrix();
    }

    private void initMatrix() {
        for (int row = 0; row < height; row++) {
            for (int col = 0; col < width; col++) {
                char c = row == 0 || row == height - 1 ? ROW_CHAR :
                    col == 0 || col == width - 1 ? COL_CHAR : BLANK_CHAR;
                this.matrix[row][col] = c;
            }
        }
    }

    public void draw() {
        for (int row = 0; row < height; row++) {
            for (int col = 0; col < width; col++) {
                System.out.print(this.matrix[row][col]);
            }
            System.out.println();
        }
    }

    public void setValue(int x, int y, Character c) {
        if (isInside(x, y)) {
            this.matrix[y][x] = c;
        }
    }

    public Character getValue(int x, int y) {
        if (isInside(x, y)) {
            return this.matrix[y][x];
        }
        return null;
    }

    private boolean isInside(int x, int y) {
        return x > 0 && x < width - 1 && y > 0 && y < height - 1;
    }
}
