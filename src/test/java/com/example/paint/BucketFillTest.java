package com.example.paint;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class BucketFillTest {

    private final static Character BLANK = ' ';
    private static final PrintStream normalOut = System.out;

    @BeforeEach
    public void setUpStreams() {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outputStream));
    }

    @AfterEach
    public void restoreStreams() {
        System.setOut(normalOut);
    }

    @Mock
    private Canvas canvas;

    @Test
    void drawOnEmptyCanvas() {
        spy(new BucketFill(1, 2, 'o')).draw(null);
        verify(canvas, never()).draw();
    }

    @ParameterizedTest
    @ValueSource(chars = {'o', 'c'})
    void draw(Character character) {
        when(canvas.getValue(anyInt(), anyInt())).thenReturn(character);
        when(canvas.getValue(intThat(x-> x == 3), intThat(y -> y == 5))).thenReturn(BLANK, character);
        when(canvas.getValue(intThat(x-> x == 4), intThat(y -> y == 5))).thenReturn(BLANK, character);
        when(canvas.getValue(intThat(x-> x == 5), intThat(y -> y == 5))).thenReturn(BLANK, character);
        when(canvas.getValue(intThat(x-> x == 3), intThat(y -> y == 6))).thenReturn(BLANK, character);
        when(canvas.getValue(intThat(x-> x == 4), intThat(y -> y == 6))).thenReturn(BLANK, character);
        when(canvas.getValue(intThat(x-> x == 5), intThat(y -> y == 6))).thenReturn(BLANK, character);
        spy(new BucketFill(4, 5, character)).draw(canvas);
        verify(canvas, times(6)).setValue(anyInt(), anyInt(), anyChar());
        verify(canvas).draw();
    }
}