package com.example.paint;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.mockito.ArgumentMatchers.anyChar;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class RectangleTest {
    private static final PrintStream normalOut = System.out;

    @BeforeEach
    public void setUpStreams() {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outputStream));
    }

    @AfterEach
    public void restoreStreams() {
        System.setOut(normalOut);
    }

    @Mock
    private Canvas canvas;

    @Test
    void drawOnEmptyCanvas() {
        spy(new Rectangle(1, 2, 1, 4)).draw(null);
        verify(canvas, never()).draw();
    }

    @ParameterizedTest
    @CsvSource({"1,5,3,4", "9,3,4,7", "4,5,4,6", "7,8,9,8"})
    void drawInvalidRectangle(int x1, int y1, int x2, int y2) {
        spy(new Rectangle(x1, y1, x2, y2)).draw(canvas);

        verify(canvas, never()).setValue(anyInt(), anyInt(), anyChar());
        verify(canvas, never()).draw();
    }

    @ParameterizedTest
    @CsvSource({"2,5,7,9", "7,9,2,5"})
    void drawValidRectangle(int x1, int y1, int x2, int y2) {
        spy(new Rectangle(x1, y1, x2, y2)).draw(canvas);

        verify(canvas, times(18)).setValue(anyInt(), anyInt(), anyChar());
        verify(canvas).draw();
    }
}
