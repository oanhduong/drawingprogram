package com.example.paint;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class LineTest {

    private final static Character DEFAULT_CHAR = 'x';
    private static final PrintStream normalOut = System.out;

    @BeforeEach
    public void setUpStreams() {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outputStream));
    }

    @AfterEach
    public void restoreStreams() {
        System.setOut(normalOut);
    }

    @Mock
    private Canvas canvas;

    @Test
    void drawOnEmptyCanvas() {
        spy(new Line(1, 2, 1, 4)).draw(null);
        verify(canvas, never()).draw();
    }

    @ParameterizedTest
    @CsvSource({"1,2,3,4", "4,3,2,1"})
    void drawDiagonal(int x1, int y1, int x2, int y2) {
       spy(new Line(x1, y1, x2, y2)).draw(canvas);

       verify(canvas, never()).setValue(anyInt(), anyInt(), anyChar());
       verify(canvas, never()).draw();
    }

    @ParameterizedTest
    @CsvSource({"2,4,4,4", "4,4,2,4"})
    void drawVertical(int x1, int y1, int x2, int y2) {
        spy(new Line(x1, y1, x2, y2)).draw(canvas);

        verify(canvas, times(1)).setValue(2, 4, DEFAULT_CHAR);
        verify(canvas, times(1)).setValue(3, 4, DEFAULT_CHAR);
        verify(canvas, times(1)).setValue(4, 4, DEFAULT_CHAR);
        verify(canvas, times(1)).draw();
    }

    @ParameterizedTest
    @CsvSource({"3,6,3,8", "3,8,3,6"})
    void drawHorizon(int x1, int y1, int x2, int y2) {
        spy(new Line(x1, y1, x2, y2)).draw(canvas);

        verify(canvas, times(1)).setValue(3, 6, DEFAULT_CHAR);
        verify(canvas, times(1)).setValue(3, 7, DEFAULT_CHAR);
        verify(canvas, times(1)).setValue(3, 8, DEFAULT_CHAR);
        verify(canvas, times(1)).draw();
    }
}
