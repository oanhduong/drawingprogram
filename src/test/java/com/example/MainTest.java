package com.example;

import com.example.paint.BucketFill;
import com.example.paint.Canvas;
import com.example.paint.Line;
import com.example.paint.Rectangle;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.MockedConstruction;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class MainTest {

    private static final InputStream normalIn = System.in;
    private static final PrintStream normalOut = System.out;
    private static ByteArrayOutputStream outputStream;

    @BeforeEach
    public void setUpStreams() {
        setOutput();
    }

    @AfterEach
    public void restoreStreams() {
        System.setIn(normalIn);
        System.setOut(normalOut);
    }

    private void setInput(List<String> input) {
        String format = input.stream().collect(Collectors.joining(System.lineSeparator()));
        format = String.format("%s%s", format, System.lineSeparator());
        ByteArrayInputStream inputStream = new ByteArrayInputStream(format.getBytes(StandardCharsets.UTF_8));
        System.setIn(inputStream);
    }

   private void setOutput() {
        outputStream = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outputStream));
    }

    private String getOutput() {
        return outputStream.toString().trim().replaceAll("\\r\\n?", "\n");
    }

    @ParameterizedTest()
    @ValueSource(strings = {"C 20 4", "c 20 4"})
    void createCanvas(String cmd) {
        try (MockedConstruction<Canvas> mocked = mockConstruction(Canvas.class)){
            List<String> commands = List.of(cmd, "Q");
            setInput(commands);
            Main.main(new String[]{""});
            assertEquals(1, mocked.constructed().size());
            assertNotNull(mocked.constructed().get(0));
            verify(mocked.constructed().get(0), times(1)).draw();
        }
    }

    @ParameterizedTest()
    @ValueSource(strings = {"C a b", "C 20"})
    void createCanvasInvalid(String cmd) {
        try (MockedConstruction<Canvas> mocked = mockConstruction(Canvas.class)){
            List<String> commands = List.of(cmd, "Q");
            setInput(commands);
            Main.main(new String[]{""});
            assertEquals(0, mocked.constructed().size());
        }
    }

    @ParameterizedTest()
    @ValueSource(strings = {"L 1 2 6 2", "l 1 2 6 2"})
    void createLine(String cmd) {
        try (MockedConstruction<Line> mocked = mockConstruction(Line.class)){
            List<String> commands = List.of(cmd, "Q");
            setInput(commands);
            Main.main(new String[]{""});
            assertNotNull(mocked.constructed().get(0));
            verify(mocked.constructed().get(0), times(1)).draw(any());
        }
    }

    @ParameterizedTest()
    @ValueSource(strings = {"L a 1 c 3", "L 1 2 6"})
    void createLineInvalid(String cmd) {
        try (MockedConstruction<Line> mocked = mockConstruction(Line.class)){
            List<String> commands = List.of(cmd, "Q");
            setInput(commands);
            Main.main(new String[]{""});
            assertEquals(0, mocked.constructed().size());
        }
    }

    @ParameterizedTest()
    @ValueSource(strings = {"R 1 2 6 4", "r 1 2 6 4"})
    void createRectangle(String cmd) {
        try (MockedConstruction<Rectangle> mocked = mockConstruction(Rectangle.class)){
            List<String> commands = List.of(cmd, "Q");
            setInput(commands);
            Main.main(new String[]{""});
            assertNotNull(mocked.constructed().get(0));
            verify(mocked.constructed().get(0), times(1)).draw(any());
        }
    }

    @ParameterizedTest()
    @ValueSource(strings = {"R a b 6 4", "R 1 2"})
    void createRectangleInvalid(String cmd) {
        try (MockedConstruction<Rectangle> mocked = mockConstruction(Rectangle.class)){
            List<String> commands = List.of(cmd, "Q");
            setInput(commands);
            Main.main(new String[]{""});
            assertEquals(0, mocked.constructed().size());
        }
    }

    @ParameterizedTest()
    @ValueSource(strings = {"B 1 2 o", "b 1 2 o"})
    void createBucketFill(String cmd) {
        try (MockedConstruction<BucketFill> mocked = mockConstruction(BucketFill.class)){
            List<String> commands = List.of(cmd, "Q");
            setInput(commands);
            Main.main(new String[]{""});
            assertNotNull(mocked.constructed().get(0));
            verify(mocked.constructed().get(0), times(1)).draw(any());
        }
    }

    @ParameterizedTest()
    @ValueSource(strings = {"B a b c", "B 1 2"})
    void createBucketFillInvalid(String cmd) {
        try (MockedConstruction<BucketFill> mocked = mockConstruction(BucketFill.class)){
            List<String> commands = List.of(cmd, "Q");
            setInput(commands);
            Main.main(new String[]{""});
            assertEquals(0, mocked.constructed().size());
        }
    }

    @Test()
    void fullTest() {
        List<String> commands = List.of(
                "C 20 4",
                "L 1 2 6 2",
                "L 6 3 6 4",
                "R 14 1 18 3",
                "B 10 3 o",
                "B 10 3 c",
                "Q"
        );
        setInput(commands);
        Main.main(new String[]{""});
        assertEquals("""
                        enter command:
                        ----------------------
                        |                    |
                        |                    |
                        |                    |
                        |                    |
                        ----------------------
                        enter command:
                        ----------------------
                        |                    |
                        |xxxxxx              |
                        |                    |
                        |                    |
                        ----------------------
                        enter command:
                        ----------------------
                        |                    |
                        |xxxxxx              |
                        |     x              |
                        |     x              |
                        ----------------------
                        enter command:
                        ----------------------
                        |             xxxxx  |
                        |xxxxxx       x   x  |
                        |     x       xxxxx  |
                        |     x              |
                        ----------------------
                        enter command:
                        ----------------------
                        |oooooooooooooxxxxxoo|
                        |xxxxxxooooooox   xoo|
                        |     xoooooooxxxxxoo|
                        |     xoooooooooooooo|
                        ----------------------
                        enter command:
                        ----------------------
                        |cccccccccccccxxxxxcc|
                        |xxxxxxcccccccx   xcc|
                        |     xcccccccxxxxxcc|
                        |     xcccccccccccccc|
                        ----------------------
                        enter command:""".replaceAll("\\r\\n?", "\n"),
                getOutput());
    }
}
